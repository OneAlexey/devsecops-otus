1) Подключён SAST с помощью сканирования Semgrep. Обнаружена уязвимость CWE-89 https://gitlab.com/security9772522/devsecops-otus/-/blob/main/reports/gl-sast-report.json
Некорректная нейтрализация специальных элементов, используемых в SQL-команде.

2) Container Scanning, включающий License Scanning, - с помощью Trivy.
https://gitlab.com/security9772522/devsecops-otus/-/blob/main/reports/gl-container-scanning-report.json

3) Dependency Scanning - с помощью Gemnasium.
https://gitlab.com/security9772522/devsecops-otus/-/blob/main/reports/gl-dependency-scanning-report.json

5) Secret Detection - с помощью Gitleaks.
https://gitlab.com/security9772522/devsecops-otus/-/blob/main/reports/gl-secret-detection-report.json